CFLAGS+=-Wuninitialized -I. -fPIE

all: example-codelock example-buffer example-small

example-codelock: example-codelock.c

example-buffer: example-buffer.c

example-small: example-small.c
